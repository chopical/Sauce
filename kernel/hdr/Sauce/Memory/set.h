#ifndef __SAUCE_MEMORY_SET__
#define __SAUCE_MEMORY_SET__
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include <Sauce/Types/parameter.h>
void sauce_memory_set(const sauce_types_parameter_st parameter);
#endif