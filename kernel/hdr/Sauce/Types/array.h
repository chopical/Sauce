#ifndef __SAUCE_TYPES_ARRAY__
#define __SAUCE_TYPES_ARRAY__
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

typedef struct {
    void* ptr;
    uint32_t count_size;
    uint32_t type_size;
} sauce_types_array_st;
#endif