#ifndef __SAUCE_SERIAL_COM_INIT__
#define __SAUCE_SERIAL_COM_INIT__
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

bool sauce_serial_init_port(uint16_t port);
void sauce_serial_init();

#endif