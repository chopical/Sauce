#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include <Sauce/kernel.h>
#include <Sauce/memory.h>
#include <Sauce/serial.h>
#include <references.h>


void sauce_kernel_main(const sauce_kernel_parameter_st kernel_parameter){
    sauce_serial_init();

    sauce_serial_write_string("kernel bottom:",COM_HIGH_PORT());
    sauce_serial_write_number((uint64_t)KERNEL_BOTTOM_REFERENCE,16,COM_HIGH_PORT());
    sauce_serial_write_string("\r\nkernel top:",COM_HIGH_PORT());
    sauce_serial_write_number((uint64_t)KERNEL_TOP_REFERENCE,16,COM_HIGH_PORT());
    sauce_serial_write_string("\r\nframebuffer address:",COM_HIGH_PORT());
    sauce_serial_write_number((uint64_t)kernel_parameter.sauce_framebuffer.address,16,COM_HIGH_PORT());
    sauce_serial_write_string("\r\nframebuffer height:",COM_HIGH_PORT());
    sauce_serial_write_number(kernel_parameter.sauce_framebuffer.height,10,COM_HIGH_PORT());
    sauce_serial_write_string("\r\nframebuffer width:",COM_HIGH_PORT());
    sauce_serial_write_number(kernel_parameter.sauce_framebuffer.width,10,COM_HIGH_PORT());
    sauce_serial_write_string("\r\nframebuffer bytes per row(pitch):",COM_HIGH_PORT());
    sauce_serial_write_number(kernel_parameter.sauce_framebuffer.bytes_per_row,10,COM_HIGH_PORT());
    sauce_serial_write_string("\r\nframebuffer bits per pixel:",COM_HIGH_PORT());
    sauce_serial_write_number(kernel_parameter.sauce_framebuffer.bits_per_pixel,10,COM_HIGH_PORT());
    sauce_serial_write_string("\r\nframebuffer red mask size:",COM_HIGH_PORT());
    sauce_serial_write_number(kernel_parameter.sauce_framebuffer.red_mask_size,10,COM_HIGH_PORT());
    sauce_serial_write_string("\r\nframebuffer red mask shift:",COM_HIGH_PORT());
    sauce_serial_write_number(kernel_parameter.sauce_framebuffer.red_mask_shift,10,COM_HIGH_PORT());
    sauce_serial_write_string("\r\nframebuffer green mask size:",COM_HIGH_PORT());
    sauce_serial_write_number(kernel_parameter.sauce_framebuffer.green_mask_size,10,COM_HIGH_PORT());
    sauce_serial_write_string("\r\nframebuffer green mask shift:",COM_HIGH_PORT());
    sauce_serial_write_number(kernel_parameter.sauce_framebuffer.green_mask_shift,10,COM_HIGH_PORT());
    sauce_serial_write_string("\r\nframebuffer blue mask size:",COM_HIGH_PORT());
    sauce_serial_write_number(kernel_parameter.sauce_framebuffer.blue_mask_size,10,COM_HIGH_PORT());
    sauce_serial_write_string("\r\nframebuffer blue mask shift:",COM_HIGH_PORT());
    sauce_serial_write_number(kernel_parameter.sauce_framebuffer.blue_mask_shift,10,COM_HIGH_PORT());
    sauce_serial_write_string("\r\n",COM_HIGH_PORT());

}
