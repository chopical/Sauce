#include <Sauce/Memory/set.h>

void sauce_memory_set(const sauce_types_parameter_st parameter){
    uint8_t *pszDest = (uint8_t *)parameter.destination.ptr;
	uint8_t *pszSource = (uint8_t*)parameter.source.ptr;
    if((parameter.source.count_size*parameter.source.type_size) > (parameter.destination.count_size*parameter.destination.type_size)){
        //TODO: throw size error.
    }
    
    size_t TotalSizeInBytes = parameter.source.count_size*parameter.source.type_size;
    size_t Copies = (parameter.destination.count_size*parameter.destination.type_size)/TotalSizeInBytes;
    while(Copies){
        size_t TotalSizeInBytes = parameter.source.count_size*parameter.source.type_size;
        Copies--;
        pszSource = (uint8_t*)parameter.source.ptr;
        while(TotalSizeInBytes){
	    	if(TotalSizeInBytes/sizeof(size_t)){
	    		*((size_t*)pszDest) = *((size_t*)pszSource);
	    		pszDest+=(sizeof(size_t)/sizeof(uint8_t));
	    		pszSource+=(sizeof(size_t)/sizeof(uint8_t));
	    		TotalSizeInBytes-=(sizeof(size_t)/sizeof(uint8_t));
	    	}else if(TotalSizeInBytes/sizeof(uint32_t)){
	    		*((uint32_t*)pszDest) = *((uint32_t*)pszSource);
	    		pszDest+=(sizeof(uint32_t)/sizeof(uint8_t));
	    		pszSource+=(sizeof(uint32_t)/sizeof(uint8_t));
	    		TotalSizeInBytes-=(sizeof(uint32_t)/sizeof(uint8_t));
	    	}else if(TotalSizeInBytes/sizeof(uint16_t)){
	    		*((uint16_t*)pszDest) = *((uint16_t*)pszSource);
	    		pszDest+=(sizeof(uint16_t)/sizeof(uint8_t));
	    		pszSource+=(sizeof(uint16_t)/sizeof(uint8_t));
	    		TotalSizeInBytes-=(sizeof(uint16_t)/sizeof(uint8_t));
	    	}else{
	    		*(pszDest++)= *(pszSource++);
	    		TotalSizeInBytes--;
	    	}
	    }
    }
}

// wrapper replacement for the old way, because gcc and clang are expecting the function to exist.
void *memset(void *dest, int c, size_t n) {
    void* src = (void*)&c;
	sauce_types_parameter_st param;
    sauce_types_array_st source;
    sauce_types_array_st destination;
    source.count_size=n;
    source.type_size=1;
    source.ptr=(void*)src;
    param.source=source;
    destination.count_size=n;
    destination.type_size=1;
    destination.ptr=(void*)dest;
    param.destination=destination;
	sauce_memory_set(param);
    return dest;
}

/*/ //the old way, here purely as a reference.
void *memset(void *s, int c, size_t n) {
    uint8_t *p = (uint8_t *)s;

    for (size_t i = 0; i < n; i++) {
        p[i] = (uint8_t)c;
    }

    return s;
}
/*/