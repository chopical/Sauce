#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include <limine.h>
#include <Sauce/kernel.h>

// Set the base revision to 2, this is recommended as this is the latest
// base revision described by the Limine boot protocol specification.
// See specification for further info.

__attribute__((used, section(".requests")))
static volatile LIMINE_BASE_REVISION(2);

// The Limine requests can be placed anywhere, but it is important that
// the compiler does not optimise them away, so, usually, they should
// be made volatile or equivalent, _and_ they should be accessed at least
// once or marked as used with the "used" attribute as done here.

__attribute__((used, section(".requests")))
static volatile struct limine_framebuffer_request framebuffer_request = {
    .id = LIMINE_FRAMEBUFFER_REQUEST,
    .revision = 0
};

__attribute__((used, section(".requests")))
static volatile struct limine_stack_size_request stack_request = {
    .id = LIMINE_STACK_SIZE_REQUEST,
    .revision = 0,
    .stack_size = (64*1024*1024) //64 KiB
};

__attribute__((used, section(".requests")))
static volatile struct limine_memmap_request memmap_request = {
    .id = LIMINE_MEMMAP_REQUEST,
    .revision = 0
};


// Finally, define the start and end markers for the Limine requests.
// These can also be moved anywhere, to any .c file, as seen fit.

__attribute__((used, section(".requests_start_marker")))
static volatile LIMINE_REQUESTS_START_MARKER;

__attribute__((used, section(".requests_end_marker")))
static volatile LIMINE_REQUESTS_END_MARKER;


// Halt and catch fire function.
static void hcf(void) {
    for (;;) {
#if defined (__x86_64__)
        asm volatile("hlt");
#elif defined (__aarch64__) || defined (__riscv)
        asm volatile("wfi");
#elif defined (__loongarch64)
        asm volatile("idle 0");
#endif
    }
}

// The following will be our kernel's entry point.
// If renaming kmain() to something else, make sure to change the
// linker script accordingly.

extern void kmain(void);

void kinit(void) {
    // Ensure the bootloader actually understands our base revision (see spec).
    if (LIMINE_BASE_REVISION_SUPPORTED == false) {
        hcf();
    }

    // Ensure we got a framebuffer.
    if (framebuffer_request.response == NULL
     || framebuffer_request.response->framebuffer_count < 1) {
        hcf();
    }

    sauce_kernel_parameter_st kernel_parameter;

    //framebuffer
        struct limine_framebuffer *framebuffer = framebuffer_request.response->framebuffers[0];
        sauce_framebuffer_st sauce_framebuffer;
        sauce_framebuffer.address = (void*)framebuffer->address;
        sauce_framebuffer.width = framebuffer->width;
        sauce_framebuffer.height = framebuffer->height;
        sauce_framebuffer.bytes_per_row = framebuffer->pitch;
        sauce_framebuffer.bits_per_pixel = framebuffer->bpp;
        sauce_framebuffer.red_mask_size = framebuffer->red_mask_size;
        sauce_framebuffer.red_mask_shift = framebuffer->red_mask_shift;
        sauce_framebuffer.green_mask_size = framebuffer->green_mask_size;
        sauce_framebuffer.green_mask_shift = framebuffer->green_mask_shift;
        sauce_framebuffer.blue_mask_size = framebuffer->blue_mask_size;
        sauce_framebuffer.blue_mask_shift = framebuffer->blue_mask_shift;
        kernel_parameter.sauce_framebuffer=sauce_framebuffer;
    //mmap
        sauce_memmap_st sauce_memmap;
        sauce_memmap.entry_count = memmap_request.response->entry_count;
        sauce_memmap.entries = memmap_request.response->entries;
        kernel_parameter.sauce_memmap=sauce_memmap;
    

    sauce_kernel_main(kernel_parameter);

    // We're done, just hang...
    hcf();
}
