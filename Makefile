

.PHONY: clean all run prep

clean:
	cd limine ;make distclean clean

all: prep
	cd limine ;make all

run:
	cd limine ;make run

prep: clean
	rm -frv limine/kernel/src/* limine/kernel/hdr/* ;cp -r limine/kernel/src-limine/* limine/kernel/src/ ;cp -r limine/kernel/hdr-limine/* limine/kernel/hdr/
	cp -r kernel/hdr/* limine/kernel/hdr/
	cp -r kernel/src/* limine/kernel/src/
