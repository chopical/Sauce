#ifndef __SAUCE_ASM_WRAPPERS__
#define __SAUCE_ASM_WRAPPERS__
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

void outb(uint16_t port, uint8_t val);
uint8_t inb(uint16_t port);
void io_wait(void);
bool are_interrupts_enabled();
unsigned long save_irqdisable(void);
void irqrestore(unsigned long flags);

#endif