#ifndef __SAUCE_KERNEL__
#define __SAUCE_KERNEL__


typedef struct {
    void* address;
    uint64_t width;
    uint64_t height;
    uint64_t bytes_per_row;//"pitch"
    uint16_t bits_per_pixel;
    uint8_t red_mask_size;
    uint8_t red_mask_shift;
    uint8_t green_mask_size;
    uint8_t green_mask_shift;
    uint8_t blue_mask_size;
    uint8_t blue_mask_shift;
} sauce_framebuffer_st;

typedef struct {
    uint64_t entry_count;
    void* entries;
} sauce_memmap_st;

typedef struct {
    sauce_framebuffer_st sauce_framebuffer;
    sauce_memmap_st sauce_memmap;
} sauce_kernel_parameter_st;

void sauce_kernel_main(const sauce_kernel_parameter_st kernel_parameter);

#endif