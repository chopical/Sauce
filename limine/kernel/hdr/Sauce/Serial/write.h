#ifndef __SAUCE_SERIAL_WRITE__
#define __SAUCE_SERIAL_WRITE__
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

int sauce_serial_is_transmit_empty(const uint16_t port);
void sauce_serial_write_char(const char a,const uint16_t port);
void sauce_serial_write_string(const char* str,const uint16_t port);
void sauce_serial_write_number(const size_t value,const size_t base,const uint16_t port);

#endif