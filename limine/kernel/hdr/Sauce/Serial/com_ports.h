#ifndef __SAUCE_SERIAL_COM_PORTS__
#define __SAUCE_SERIAL_COM_PORTS__
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

extern const uint16_t COM1_PORT;extern bool COM1_GOOD;
extern const uint16_t COM2_PORT;extern bool COM2_GOOD;
extern const uint16_t COM3_PORT;extern bool COM3_GOOD;
extern const uint16_t COM4_PORT;extern bool COM4_GOOD;
extern const uint16_t COM5_PORT;extern bool COM5_GOOD;
extern const uint16_t COM6_PORT;extern bool COM6_GOOD;
extern const uint16_t COM7_PORT;extern bool COM7_GOOD;
extern const uint16_t COM8_PORT;extern bool COM8_GOOD;

inline uint16_t COM_HIGH_PORT(){
    if(COM8_GOOD)return COM8_PORT;
    if(COM7_GOOD)return COM7_PORT;
    if(COM6_GOOD)return COM6_PORT;
    if(COM5_GOOD)return COM5_PORT;
    if(COM4_GOOD)return COM4_PORT;
    if(COM3_GOOD)return COM3_PORT;
    if(COM2_GOOD)return COM2_PORT;
    if(COM1_GOOD)return COM1_PORT;
    //TODO: throw error, for new we just return COM1_PORT just to have something that wont effect any other systems
    return COM1_PORT;
}

inline uint16_t COM_LOW_PORT(){
    if(COM1_GOOD)return COM1_PORT;
    if(COM2_GOOD)return COM2_PORT;
    if(COM3_GOOD)return COM3_PORT;
    if(COM4_GOOD)return COM4_PORT;
    if(COM5_GOOD)return COM5_PORT;
    if(COM6_GOOD)return COM6_PORT;
    if(COM7_GOOD)return COM7_PORT;
    if(COM8_GOOD)return COM8_PORT;
    //TODO: throw error, for new we just return COM8_PORT just to have something that wont effect any other systems
    return COM8_PORT;
}

#endif