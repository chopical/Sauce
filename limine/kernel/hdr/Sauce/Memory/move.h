#ifndef __SAUCE_MEMORY_MOVE__
#define __SAUCE_MEMORY_MOVE__
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include <Sauce/Types/parameter.h>
void sauce_memory_move(const sauce_types_parameter_st parameter);
#endif