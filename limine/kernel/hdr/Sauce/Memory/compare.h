#ifndef __SAUCE_MEMORY_COMPARE__
#define __SAUCE_MEMORY_COMPARE__
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include <Sauce/Types/parameter.h>
int sauce_memory_compare(const sauce_types_parameter_st parameter);
#endif