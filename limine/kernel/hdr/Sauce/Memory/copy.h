#ifndef __SAUCE_MEMORY_COPY__
#define __SAUCE_MEMORY_COPY__
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include <Sauce/Types/parameter.h>
void sauce_memory_copy(const sauce_types_parameter_st parameter);
#endif