#ifndef __SAUCE_TYPES_PARAMETER__
#define __SAUCE_TYPES_PARAMETER__
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include <Sauce/Types/array.h>

typedef struct {
    sauce_types_array_st source;
    sauce_types_array_st destination;
} sauce_types_parameter_st;

#endif