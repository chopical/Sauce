#include <Sauce/Memory/compare.h>

int sauce_memory_compare(const sauce_types_parameter_st parameter){
    if((parameter.source.count_size*parameter.source.type_size) != (parameter.destination.count_size*parameter.destination.type_size)){
        return (parameter.source.count_size*parameter.source.type_size) < (parameter.destination.count_size*parameter.destination.type_size) ? -1 : 1;
    }
    uint8_t *p1 = (uint8_t *)parameter.source.ptr;
	uint8_t *p2 = (uint8_t*)parameter.destination.ptr;
    size_t n = (parameter.source.count_size*parameter.source.type_size);
    for (size_t i = 0; i < n; i++) {
        if (p1[i] != p2[i]) {
            return p1[i] < p2[i] ? -1 : 1;
        }
    }
}


// wrapper replacement for the old way, because gcc and clang are expecting the function to exist.
int memcmp(const void *s1, const void *s2, size_t n){
    sauce_types_parameter_st param;
    sauce_types_array_st source;
    sauce_types_array_st destination;
    source.count_size=n;
    source.type_size=1;
    source.ptr=(void*)s1;
    param.source=source;
    destination.count_size=n;
    destination.type_size=1;
    destination.ptr=(void*)s2;
    param.destination=destination;
    return sauce_memory_compare(param);
}

/*/ //the old way, here purely as a reference.
int memcmp(const void *s1, const void *s2, size_t n) {
    const uint8_t *p1 = (const uint8_t *)s1;
    const uint8_t *p2 = (const uint8_t *)s2;

    for (size_t i = 0; i < n; i++) {
        if (p1[i] != p2[i]) {
            return p1[i] < p2[i] ? -1 : 1;
        }
    }

    return 0;
}
/*/