#include <Sauce/Memory/copy.h>

void sauce_memory_copy(const sauce_types_parameter_st parameter){
    uint8_t *pszDest = (uint8_t *)parameter.destination.ptr;
	uint8_t *pszSource =(uint8_t*)parameter.source.ptr;
    if((parameter.source.count_size*parameter.source.type_size) > (parameter.destination.count_size*parameter.destination.type_size)){
        //TODO: throw size error.
    }
    size_t TotalSizeInBytes = parameter.source.count_size*parameter.source.type_size;
    while(TotalSizeInBytes){
		if(TotalSizeInBytes/sizeof(size_t)){
			*((size_t*)pszDest) = *((size_t*)pszSource);
			pszDest+=(sizeof(size_t)/sizeof(uint8_t));
			pszSource+=(sizeof(size_t)/sizeof(uint8_t));
			TotalSizeInBytes-=(sizeof(size_t)/sizeof(uint8_t));
		}else if(TotalSizeInBytes/sizeof(uint32_t)){
			*((uint32_t*)pszDest) = *((uint32_t*)pszSource);
			pszDest+=(sizeof(uint32_t)/sizeof(uint8_t));
			pszSource+=(sizeof(uint32_t)/sizeof(uint8_t));
			TotalSizeInBytes-=(sizeof(uint32_t)/sizeof(uint8_t));
		}else if(TotalSizeInBytes/sizeof(uint16_t)){
			*((uint16_t*)pszDest) = *((uint16_t*)pszSource);
			pszDest+=(sizeof(uint16_t)/sizeof(uint8_t));
			pszSource+=(sizeof(uint16_t)/sizeof(uint8_t));
			TotalSizeInBytes-=(sizeof(uint16_t)/sizeof(uint8_t));
		}else{
			*(pszDest)= *(pszSource);
			pszDest++;
			pszSource++;
			TotalSizeInBytes--;
		}
	}
}


// wrapper replacement for the old way, because gcc and clang are expecting the function to exist.
void *memcpy(void *dest, const void *src, size_t n){
	sauce_types_parameter_st param;
    sauce_types_array_st source;
    sauce_types_array_st destination;
    source.count_size=n;
    source.type_size=1;
    source.ptr=(void*)src;
    param.source=source;
    destination.count_size=n;
    destination.type_size=1;
    destination.ptr=(void*)dest;
    param.destination=destination;
	sauce_memory_copy(param);
	return dest;
}

/*/ //the old way, here purely as a reference.
void *memcpy(void *dest, const void *src, size_t n) {
    uint8_t *pdest = (uint8_t *)dest;
    const uint8_t *psrc = (const uint8_t *)src;

    for (size_t i = 0; i < n; i++) {
        pdest[i] = psrc[i];
    }

    return dest;
}
/*/

