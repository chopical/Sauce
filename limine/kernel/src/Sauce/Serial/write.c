#include <Sauce/Serial/write.h>

#include <Sauce/asm_wrappers.h>

int sauce_serial_is_transmit_empty(const uint16_t port){
   return inb(port + 5) & 0x20;
}
void sauce_serial_write_char(const char a,const uint16_t port) {
   while (sauce_serial_is_transmit_empty(port) == 0);
   outb(port,a);
}
void sauce_serial_write_string(const char* str,const uint16_t port){
    char* cptr = (char*)str;
    while((*cptr) != '\0'){
        sauce_serial_write_char((*cptr),port);
        cptr++;
    }
}

const char * digits = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ+-";
void sauce_serial_write_number(const size_t value,const size_t base,const uint16_t port){
    if ( ( value / base ) != 0 ){
        sauce_serial_write_number( value / base, base, port);
    }
    sauce_serial_write_char(digits[value % base],port);
}