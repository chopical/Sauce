/*/resolution: 1024x768x32/*/
#include "kernel.h"
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_main.h>
#include <pthread.h>
#include <unistd.h>


const size_t ScreenWidth = 1024;
const size_t ScreenHeight = 768;
const size_t BPP = sizeof(uint32_t);
const uint8_t RedMaskSize = 8;
const uint8_t GreenMaskSize = 8;
const uint8_t BlueMaskSize = 8;
const uint8_t AlphaMaskSize = 8;
const uint8_t AlphaMaskShift = 24;
const uint8_t RedMaskShift = 16;
const uint8_t GreenMaskShift = 8;
const uint8_t BlueMaskShift = 0;

uint8_t* ScreenBuffer = NULL;
const size_t ScreenSize = BPP*ScreenWidth*ScreenHeight;
SDL_Window* window = NULL;
SDL_Renderer* renderer = NULL;
SDL_Texture* texture = NULL;

bool Running = true;

void* flipper(void* vargp){
    SDL_Init(SDL_INIT_VIDEO);
    window = SDL_CreateWindow("FauxScreen", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, ScreenWidth, ScreenHeight, 0);
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED );
    texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STATIC, ScreenWidth, ScreenHeight);
    while(Running){
        sleep(1); // 1/fps
        SDL_RenderClear(renderer);
        for(size_t iY=0;iY<ScreenHeight;iY++){
            for(size_t iX=0;iX<ScreenWidth;iX++){
                //((uint32_t*)kernel_parameter.sauce_framebuffer.address)[(y * kernel_parameter.sauce_framebuffer.bytes_per_row) + x] |= color;
                uint32_t pixel = ((uint32_t*)ScreenBuffer)[(iY * (ScreenWidth*(BPP/8))) + iX];
                uint8_t red = (uint8_t)(pixel >> RedMaskShift);
                uint8_t green = (uint8_t)(pixel >> GreenMaskShift);
                uint8_t blue = (uint8_t)(pixel >> BlueMaskShift);
                uint8_t alpha = (uint8_t)(pixel >> AlphaMaskShift);
                SDL_SetRenderDrawColor(renderer, red,green,blue,alpha);
                SDL_RenderDrawPoint(renderer, iY, iX);
            }
        }
        SDL_RenderPresent(renderer);
    }
    SDL_DestroyTexture(texture);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
    return NULL;
}
void* entry(void* vargp){
    sauce_framebuffer_st sauce_framebuffer = {
        .address = ScreenBuffer,
        .width = ScreenWidth,
        .height = ScreenWidth,
        .bytes_per_row = (ScreenWidth*(BPP/8)),
        .bits_per_pixel = (uint16_t)BPP,
        .red_mask_size = 8,
        .red_mask_shift = 16,
        .green_mask_size = 8,
        .green_mask_shift = 8,
        .blue_mask_size = 8,
        .blue_mask_shift = 0,
    };
    sauce_kernel_parameter_st sauce_kernel_parameter = {
        .sauce_framebuffer = sauce_framebuffer,
    };
    sauce_kernel_main(sauce_kernel_parameter);
    return NULL;
}

int main(int argc,char* argv[]){
    ScreenBuffer = malloc(ScreenSize);
    memset(ScreenBuffer,0,ScreenSize);
    
    pthread_t entry_thread_id;
    pthread_create(&entry_thread_id, NULL, entry, NULL);
    pthread_t flipper_thread_id;
    pthread_create(&flipper_thread_id, NULL, flipper, NULL);

    printf("Press enter to exit...");
    getchar();
    Running = false;
    pthread_join(flipper_thread_id, NULL);
    pthread_join(entry_thread_id, NULL);
    exit(0);
}

