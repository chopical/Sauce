#include "kernel.h"

/*/debug/*/
    #include <stdio.h>

void PutPixel(size_t x,size_t y,uint8_t red,uint8_t green,uint8_t blue,uint8_t alpha,const sauce_kernel_parameter_st kernel_parameter){
    uint8_t AlphaMaskShift=kernel_parameter.sauce_framebuffer.red_mask_size+kernel_parameter.sauce_framebuffer.green_mask_size+kernel_parameter.sauce_framebuffer.blue_mask_size;
    uint32_t Alpha = (((uint32_t)alpha) << AlphaMaskShift);
    uint32_t color = 0x00000000|Alpha;
    ((uint32_t*)kernel_parameter.sauce_framebuffer.address)[(y * kernel_parameter.sauce_framebuffer.bytes_per_row) + x] = color;
    
    uint32_t Red = (((uint32_t)red) << kernel_parameter.sauce_framebuffer.red_mask_shift);
    uint32_t Green = (((uint32_t)green) << kernel_parameter.sauce_framebuffer.green_mask_shift);
    uint32_t Blue = (((uint32_t)blue) << kernel_parameter.sauce_framebuffer.blue_mask_shift);
    color = Red|Green|Blue|Alpha;

    printf("\tPixel[%lu,%lu,%.8x,%.8x,%.8x,%.8x]:%.8x\r\n",x,y,Red,Green,Blue,Alpha,color);
    ((uint32_t*)kernel_parameter.sauce_framebuffer.address)[(y * kernel_parameter.sauce_framebuffer.bytes_per_row) + x] |= color;
}

void sauce_kernel_main(const sauce_kernel_parameter_st kernel_parameter){
    for(size_t iY=0;iY < kernel_parameter.sauce_framebuffer.height;iY++){
        for(size_t iX=0;iX < kernel_parameter.sauce_framebuffer.width;iX++){
            PutPixel(iX,iY,25,25,25,255,kernel_parameter);
        }
   }
   for(size_t iY=0;iY < kernel_parameter.sauce_framebuffer.height;iY++){
        PutPixel(iY,iY,255,255,255,255,kernel_parameter);
   }
}